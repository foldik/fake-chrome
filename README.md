## Fake Chrome

Mielőtt belevágnánk magunkat a web hatalmas világába megnézünk egy kis alkalmazást, ami szimulálja egy limitált szintig, hogy mi is történik a weben. Nézzük hát a __Fake Chrome__ alkalmazást, ami eljátssza, mintha mi url-eket hívogatnánk a böngészőből, így látogatva különböző weblapokat.

![Fake Chrome használat](images/usage.gif)

Az alkalmazásban megjelenő főbb koncepciók:
- Domain feloldás
- Servlet-ek
- HTTP kérések
- Filter-ek
- URL mapping
- Header

#### Működés

Az alkalmazás ```src/main/resources/domain``` mappájában kell felvennünk különböző weblapokat szimuláló ```csv``` fájlokat. Az így felvett domain-ek url-jeit hívogathatjuk.
Például ha felveszünk egy ```mypage.com.csv``` fájlt az alábbi tartalommal:

```
Ip Address;   Port
10.23.10.123; 45
Path;           Method; Headers;                            Return value; Filter
/stayAwesome;   POST;   Content-type: text/plain;           Awesome %s;   LOG_REQUEST,NOT_EMPTY_PARAMETER
```

Akkor meghívhatjuk a ```mypage.com/stayAwesome``` oldalt ```POST``` metódussal és __egy__ paraméterrel.
