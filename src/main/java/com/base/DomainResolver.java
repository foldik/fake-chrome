package com.base;

import com.base.model.Domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class DomainResolver {

    private final Map<String, Domain> domains;

    public DomainResolver(Map<String, Domain> domains) {
        this.domains = new HashMap<>(Objects.requireNonNull(domains, "domain must not be null"));
    }

    public Domain findDomainByUrl(String url) {
        String domainUrl = getDomainUrl(url);
        Domain domain = domains.get(domainUrl);
        if (domain == null) {
            throw new IllegalArgumentException("Unknown domain: " + domainUrl);
        }
        return domain;
    }

    private String getDomainUrl(String url) {
        return url.substring(0, url.indexOf("/"));
    }
}
