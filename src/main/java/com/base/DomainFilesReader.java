package com.base;

import com.base.filters.Filter;
import com.base.model.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class DomainFilesReader {

    private final String separator;
    private final String listSeparator;
    private final Map<String, Filter> filterMap;

    public DomainFilesReader(String separator, String listSeparator, Map<String, Filter> filterMap) {
        this.separator = Objects.requireNonNull(separator, "separator must not be null");
        this.listSeparator = Objects.requireNonNull(listSeparator, "listSeparator must not be null");
        this.filterMap = Objects.requireNonNull(filterMap, "filterMap must not be null");
    }

    public Map<String, Domain> parseDomains(String folder) throws IOException {
        System.out.println("Reading domain files ...");
        List<Path> domainDefinitionFiles = getDomainDefinitionFiles(folder);
        Map<String, Domain> domains = new HashMap<>();
        for (Path domainFile : domainDefinitionFiles) {
            List<String> lines = Files.readAllLines(domainFile, StandardCharsets.UTF_8);

            String[] domainProperties = lines.get(1).split(separator);
            String ip = domainProperties[0].trim();
            int port = Integer.parseInt(domainProperties[1].trim());

            Map<ResourceKey, List<Filter>> filters = parseFilters(lines.subList(3, lines.size()));
            Map<ResourceKey, Resource> resources = parseResources(lines.subList(3, lines.size()));

            String domainName = getDomainName(domainFile);
            Domain domain = new Domain(domainName, ip, port, new Servlets(filters, resources));
            domains.put(domainName, domain);

            System.out.println(System.lineSeparator() + domain.toString() + System.lineSeparator());
        }
        return domains;
    }

    private List<Path> getDomainDefinitionFiles(String folder) throws IOException {
        return Files.list(Paths.get(folder))
                .filter(p -> p.toFile().getName().endsWith(".csv"))
                .collect(Collectors.toList());
    }

    private String getDomainName(Path domainFile) {
        String fileName = domainFile.toFile().getName();
        return fileName.substring(0, fileName.indexOf(".csv"));
    }

    private Map<ResourceKey, Resource> parseResources(List<String> lines) {
        Map<ResourceKey, Resource> resources = new HashMap<>();
        for (String resourceLine : lines) {
            String[] values = resourceLine.split(separator);
            ResourceKey key = new ResourceKey(values[0].trim(), RequestMethod.valueOf(values[1].trim()));
            Resource resource = new Resource(values[2].trim(), values[3].trim());
            resources.put(key, resource);
        }
        return resources;
    }

    private Map<ResourceKey, List<Filter>> parseFilters(List<String> lines) {
        Map<ResourceKey, List<Filter>> resources = new HashMap<>();
        for (String resourceLine : lines) {
            String[] values = resourceLine.split(separator);
            ResourceKey key = new ResourceKey(values[0].trim(), RequestMethod.valueOf(values[1].trim()));
            String[] filterValues = values[4].split(listSeparator);
            List<Filter> filters = new ArrayList<>();
            for (String filter : filterValues) {
                if (filterMap.containsKey(filter.trim())) {
                    filters.add(filterMap.get(filter.trim()));
                }
            }
            resources.put(key, filters);
        }
        return resources;
    }
}
