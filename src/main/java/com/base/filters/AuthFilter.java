package com.base.filters;

import com.base.model.ClientException;
import com.base.model.Request;

import java.util.Objects;

public class AuthFilter implements Filter {

    private final String password;

    public AuthFilter(String password) {
        this.password = Objects.requireNonNull(password, "password must not be null");
    }

    @Override
    public void doFilter(Request request) {
        if (!request.getParameter().isPresent() || !password.equals(request.getParameter().get())) {
            throw new ClientException("Authentication failed. You are not allowed to see this page");
        } else {
            System.out.println("Authentication success.");
        }
    }
}
