package com.base.filters;

import com.base.model.ClientException;
import com.base.model.Request;

public class NotEmptyParameterFilter implements Filter {

    @Override
    public void doFilter(Request request) {
        if (!request.getParameter().isPresent()) {
            throw new ClientException("Empty pramater. Session id [ " + request.getSessionId() + " ]");
        }
    }
}
