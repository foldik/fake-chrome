package com.base.filters;

import com.base.model.Request;

public class LoggingFilter implements Filter {

    @Override
    public void doFilter(Request request) {
        System.out.println("Logging filter received: " + request);
    }
}
