package com.base.filters;

import com.base.model.Request;

public interface Filter {

    void doFilter(Request request);
}
