package com.base;

import com.base.config.Config;
import com.base.model.Domain;
import com.base.model.Request;
import com.base.model.RequestMethod;
import com.base.model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;

@Component
public class FakeChrome {

    private Tomcat tomcat;
    private DomainResolver domainResolver;

    @Autowired
    public FakeChrome(Tomcat tomcat, DomainResolver domainResolver) {
        this.tomcat = tomcat;
        this.domainResolver = domainResolver;
    }

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        FakeChrome fakeChrome = context.getBean(FakeChrome.class);
        fakeChrome.play();
    }

    public void play() {
        System.out.println("Ahoy! This is a fake chrome :D. You can visit 10 websites. Only! :D");
        Scanner scanner = new Scanner(System.in);
        int browsingCount = 0;
        while (browsingCount < 10) {
            try {
                Request request = readRequest(scanner);

                Domain domain = domainResolver.findDomainByUrl(request.getUrl());
                System.out.println();
                printDomainDetails(domain);

                Response response = tomcat.handle(domain, request);
                System.out.println(System.lineSeparator() + response + System.lineSeparator());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            browsingCount++;
        }
    }

    private String readFromConsole(String message, Scanner scanner) {
        System.out.println(message);
        return scanner.nextLine();
    }

    private Request readRequest(Scanner scanner) {
        String sessionId = UUID.randomUUID().toString();
        String url = readFromConsole("Website url: ", scanner);
        String method = readFromConsole("Method: ", scanner);
        Optional<String> parameter = readOptionalFromConsole("Parameter: ", scanner);
        return new Request(sessionId, url, RequestMethod.valueOf(method), parameter);
    }

    private Optional<String> readOptionalFromConsole(String message, Scanner scanner) {
        System.out.println(message);
        String value = scanner.nextLine();
        return value.isEmpty() ? Optional.empty() : Optional.of(value);
    }

    private void printDomainDetails(Domain domain) {
        System.out.println("Domain name: " + domain.getName());
        System.out.println("Ip address: " + domain.getIp());
        System.out.println("Port : " + domain.getPort());
    }
}
