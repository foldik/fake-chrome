package com.base.config;

import com.base.DomainFilesReader;
import com.base.DomainResolver;
import com.base.Tomcat;
import com.base.filters.AuthFilter;
import com.base.filters.Filter;
import com.base.filters.LoggingFilter;
import com.base.filters.NotEmptyParameterFilter;
import com.base.model.Domain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan(basePackages = "com.base")
@PropertySource("classpath:application.properties")
public class Config {

    @Autowired
    private Environment environment;

    @Bean
    public Tomcat tomcat() {
        return new Tomcat();
    }

    @Bean
    public DomainResolver domainResolver() {
        try {
            Map<String, Domain> domains = domainFilesReader().parseDomains(prop("domain.registrations.folder"));
            return new DomainResolver(domains);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Bean
    public DomainFilesReader domainFilesReader() {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("LOG_REQUEST", loggingFilter());
        filterMap.put("NOT_EMPTY_PARAMETER", notEmptyParameterFilter());
        filterMap.put("AUTH", authFilter());
        return new DomainFilesReader(prop("csv.separator"), prop("csv.list.separator"), filterMap);
    }

    @Bean
    public Filter loggingFilter() {
        return new LoggingFilter();
    }

    @Bean
    public Filter notEmptyParameterFilter() {
        return new NotEmptyParameterFilter();
    }

    @Bean
    public Filter authFilter() {
        return new AuthFilter(prop("auth.password"));
    }

    private String prop(String key) {
        return environment.getRequiredProperty(key);
    }
}
