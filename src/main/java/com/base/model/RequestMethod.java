package com.base.model;

public enum RequestMethod {

    GET,
    POST,
    PUT,
    DELETE,
    HEAD,
    TRACE
}
