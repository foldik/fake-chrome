package com.base.model;

import java.util.Objects;

public final class Domain {

    private final String name;
    private final String ip;
    private final int port;
    private final Servlets servlets;


    public Domain(String name, String ip, int port, Servlets servlets) {
        this.name = Objects.requireNonNull(name, "name must not be null");
        this.ip = Objects.requireNonNull(ip, "ip must not be null");
        this.port = port;
        this.servlets = Objects.requireNonNull(servlets, "servlets must not be null");
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public Servlets getServlets() {
        return servlets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Domain domain = (Domain) o;
        return port == domain.port &&
                Objects.equals(name, domain.name) &&
                Objects.equals(ip, domain.ip) &&
                Objects.equals(servlets, domain.servlets);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, ip, port, servlets);
    }

    @Override
    public String toString() {
        return "Domain{" +
                "name='" + name + '\'' +
                ", ip='" + ip + '\'' +
                ", port=" + port +
                ", servlets=" + servlets +
                '}';
    }
}
