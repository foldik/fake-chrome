package com.base.model;

import java.util.Objects;

public class Response {

    private final String header;
    private final String content;

    public Response(String header, String content) {
        this.header = Objects.requireNonNull(header, "header must not be null");
        this.content = Objects.requireNonNull(content, "content must not be null");
    }

    public String getHeader() {
        return header;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Response response = (Response) o;
        return Objects.equals(header, response.header) &&
                Objects.equals(content, response.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, content);
    }

    @Override
    public String toString() {
        return "Response{" +
                "header='" + header + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
