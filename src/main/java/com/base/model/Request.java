package com.base.model;

import java.util.Objects;
import java.util.Optional;

public final class Request {

    private final String sessionId;
    private final String url;
    private final RequestMethod method;
    private final Optional<String> parameter;

    public Request(String sessionId, String url, RequestMethod method, Optional<String> parameter) {
        this.sessionId = Objects.requireNonNull(sessionId, "sessionId must not be null");
        this.url = Objects.requireNonNull(url, "url must not be, null");
        this.method = Objects.requireNonNull(method, "method must not be null");
        this.parameter = Objects.requireNonNull(parameter, "parameter must not be null");
    }

    public String getSessionId() {
        return sessionId;
    }

    public String getUrl() {
        return url;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public Optional<String> getParameter() {
        return parameter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Request request = (Request) o;
        return Objects.equals(sessionId, request.sessionId) &&
                Objects.equals(url, request.url) &&
                method == request.method &&
                Objects.equals(parameter, request.parameter);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sessionId, url, method, parameter);
    }

    @Override
    public String toString() {
        return "Request{" +
                "sessionId='" + sessionId + '\'' +
                ", url='" + url + '\'' +
                ", method=" + method +
                ", parameter=" + parameter +
                '}';
    }
}
