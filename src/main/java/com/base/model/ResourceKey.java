package com.base.model;

import java.util.Objects;

public final class ResourceKey {

    private final String path;
    private final RequestMethod method;

    public ResourceKey(String path, RequestMethod method) {
        this.path = Objects.requireNonNull(path, "path must not be null");
        this.method = Objects.requireNonNull(method, "method must not be null");
    }

    public String getPath() {
        return path;
    }

    public RequestMethod getMethod() {
        return method;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceKey that = (ResourceKey) o;
        return Objects.equals(path, that.path) &&
                method == that.method;
    }

    @Override
    public int hashCode() {
        return Objects.hash(path, method);
    }

    @Override
    public String toString() {
        return "ResourceKey{" +
                "path='" + path + '\'' +
                ", method=" + method +
                '}';
    }
}
