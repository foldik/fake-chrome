package com.base.model;

import java.util.Objects;

public final class Resource {

    private final String header;
    private final String content;

    public Resource(String header, String content) {
        this.header = Objects.requireNonNull(header, "header must not be null");
        this.content = Objects.requireNonNull(content, "content must not be null");
    }

    public String getHeader() {
        return header;
    }

    public String getContent() {
        return content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resource resource = (Resource) o;
        return Objects.equals(header, resource.header) &&
                Objects.equals(content, resource.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, content);
    }

    @Override
    public String toString() {
        return "Resource{" +
                "header='" + header + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
