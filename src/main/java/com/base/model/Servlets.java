package com.base.model;

import com.base.filters.Filter;

import java.util.*;

public final class Servlets {

    private final Map<ResourceKey, List<Filter>> filters;
    private final Map<ResourceKey, Resource> resources;

    public Servlets(Map<ResourceKey, List<Filter>> filters, Map<ResourceKey, Resource> resources) {
        this.filters = Objects.requireNonNull(filters, "filters must not be null");
        this.resources = Objects.requireNonNull(resources, "resources must not be null");
    }

    public List<Filter> getFilters(ResourceKey resourceKey) {
        return filters.containsKey(resourceKey) ? filters.get(resourceKey) : Collections.emptyList();
    }

    public Optional<Resource> getResources(ResourceKey resourceKey) {
        return resources.containsKey(resourceKey) ? Optional.of(resources.get(resourceKey)) : Optional.empty();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Servlets servlets = (Servlets) o;
        return Objects.equals(filters, servlets.filters) &&
                Objects.equals(resources, servlets.resources);
    }

    @Override
    public int hashCode() {
        return Objects.hash(filters, resources);
    }

    @Override
    public String toString() {
        return "Servlets{" +
                "filters=" + filters +
                ", resources=" + resources +
                '}';
    }
}
