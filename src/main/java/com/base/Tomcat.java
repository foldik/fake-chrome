package com.base;

import com.base.filters.Filter;
import com.base.model.*;

import java.util.List;
import java.util.Optional;

public class Tomcat {

    public Response handle(Domain domain, Request request) {
        String path = getPath(request.getUrl());
        runFilterChain(domain.getServlets(), new ResourceKey(path, request.getMethod()), request);
        Resource resource = getResource(domain, new ResourceKey(path, request.getMethod()));
        String content = resolveContent(request, resource);
        return new Response(resource.getHeader(), content);
    }

    private String getPath(String url) {
        return url.substring(url.indexOf("/"));
    }

    private void runFilterChain(Servlets servlets, ResourceKey resourceKey, Request request) {
        List<Filter> filters = servlets.getFilters(resourceKey);
        for (Filter filter : filters) {
            filter.doFilter(request);
        }
    }

    private Resource getResource(Domain domain, ResourceKey resourceKey) {
        Optional<Resource> resource = domain.getServlets().getResources(resourceKey);
        if (!resource.isPresent()) {
            throw new IllegalArgumentException("Not found mapping for");
        }
        return resource.get();
    }

    private String resolveContent(Request request, Resource resource) {
        String content;
        if (request.getParameter().isPresent()) {
            content = String.format(resource.getContent(), request.getParameter().get());
        } else {
            content = resource.getContent();
        }
        return content;
    }

}
